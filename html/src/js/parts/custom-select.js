/**
 * select должен иметь класс .js-custom-select
 * Родительский контейнер должен иметь класс .field
 * В нераскрытом select отображается текст (placeholder) одним из трех способов:
 * 1) $('.field').find('label').text() - у лейбла должен быть класс js-get-label
 * 2) Текст из первого option, если он выбран по умолчанию
 * 3) Ничего не отображается
 * Условия описаны в переменной placeholder
 */

$(document).ready( () => {
    const durationSelect = 300;
    let selectList;
    let containersSelect = $('.js-custom-select');

    containersSelect.each( function () {
        let selectGap;
        let selectItem;
        let chooseItem;
        let $this = $(this);
        let labelText = $this.closest('.field').find('.js-get-label').text();
        let placeholder =  !!labelText ? `${labelText}` : $this.find('option')[0].text || '';
        let selectOption = $this.find('option');
        let selectedOption = selectOption.filter(':selected');

        $this.change(function() {
            let current = $this.val();

            if (current != 'null') {
                $this.css('border','black');
            } else {
                $this.css('color','gray');
            }
        });

        $this.hide();
        $this.wrap('<div class="custom-select"></div>');

        $('<div>', {
            class: 'custom-select__gap',
            text: placeholder
        }).insertAfter($this);

        selectGap = $this.next('.custom-select__gap');

        $('<div>', {
            class: 'custom-select__wrap',
            html: $('<ul>', {
                class: 'custom-select__list'
            })
        }).insertAfter(selectGap);

        selectList = selectGap.next('.custom-select__wrap').find('.custom-select__list');

        $('<div>', {
            class: 'js-reset-current'
        }).insertAfter(selectGap.next('.custom-select__wrap'));

        // Add li - option items
        $.each(selectOption, index => {
            $('<li>', {
                class: 'custom-select__item',
                html: $('<span>', {
                    text: selectOption.eq(index).text(),
                    class: 'custom-select__text'
                })
            })
                .attr('data-value', selectOption.eq(index).val())
                .appendTo(selectList);
        });

        selectItem = selectList.find('li');
        selectList.slideUp(0);

        selectGap.on('click', function (event) {

            event.stopPropagation();
            let thisList = $(this).next('.custom-select__wrap').find('.custom-select__list');

            if (!$(this).hasClass('on')) {
                $(this).addClass('on');
                $(this).next('.custom-select__wrap').addClass('on');
                thisList.slideDown(durationSelect);

                selectItem.on('click', function () {
                    let box = $(this).closest('.custom-select').find('.custom-select__gap');
                    chooseItem = $(this).data('value');

                    $('select').val(chooseItem).attr('selected', 'selected');
                    selectGap.text($(this).find('span').text());
                    box.addClass('selected');

                    thisList.slideUp(durationSelect);
                    selectGap.removeClass('on');
                    $(this).next('.custom-select__wrap').removeClass('on');

                    selectItem.off();
                });
            } else {
                $(this).removeClass('on');
                $(this).next('.custom-select__wrap').removeClass('on');
                thisList.slideUp(durationSelect);
            }
        });
    });
    
    $(window).on('click', function () {
        let container = $('.custom-select__gap');

        if (!!container.length) {
            $.each(container, function () {
                selectList = $(this).next('.custom-select__wrap').find('.custom-select__list');
                selectList.slideUp(durationSelect);
                $(this).removeClass('on');
                $(this).next('.custom-select__wrap').removeClass('on');
            })
        }
    });

    $('.js-reset-select').on('click', function () {
        let containers = $(this).closest('form').find('.js-custom-select');

        if (!containers.length) return;

        $.each(containers, function () {
            let $this = $(this);
            let labelText = $this.closest('.field').find('.js-get-label').text();
            let placeholder =  !!labelText ? `${labelText}` : $this.find('option')[0].text || '';
            let drop = $this.closest('.field').find('.custom-select__gap');

            $this.val('').removeAttr('selected');
            drop.removeClass('selected');
            drop.text(placeholder);
        });
    });

    $('.js-reset-current').on('click', function () {
        let containers = $(this).closest('.custom-select').find('.js-custom-select');

        if (!containers.length) return;

        $.each(containers, function () {
            let $this = $(this);
            let labelText = $this.closest('.field').find('.js-get-label').text();
            let placeholder =  !!labelText ? `${labelText}` : $this.find('option')[0].text || '';
            let drop = $this.closest('.field').find('.custom-select__gap');

            $this.val('').removeAttr('selected');
            drop.removeClass('selected');
            drop.text(placeholder);
        });
    });

    $('.js-rating').click( function(e) {
        e.preventDefault();

        let $check = $(this).prev();

        if ($check.prop('checked')) {
            $check.prop( 'checked', false );
        } else {
            $check.prop( 'checked', true );
        }
    });

    // Работа со слайдером "отделка" на странице card-product.html
    $('.card-product__slide').on('click', function () {
        let container = $(this).closest('.card-product__slider-box');
        let nameBox = container.find('.card-product__slider-name');
        let url = $('img', this).attr('src');

        let pictureParent; //card-product__outside
        let bigImg;

        let pattern;
        let image;

        let imageUri;
        let patternUri;

        container.addClass('selected');
        nameBox.text($(this).data('color'));
        $('.card-product__slide', container).removeClass('marked');
        $(this).addClass('marked');

        image = (url.split('/')[url.split('/').length - 1]).split('-');
        imageUri = `./images/finish/${image[0]}-${image[image.length - 1]}`;
        patternUri = `./images/finish/${image[0]}-tx-${image[image.length - 1]}`;
        pictureParent = $(`.card-product__${image[0]}`); //card-product__outside || __inside
        bigImg = $('.card-product__picture-box', pictureParent);
        pattern = $(`.js-${image[0]}-picture`, pictureParent);
        pattern.attr('src', patternUri);
        pattern.parent().addClass('red');
        bigImg.attr('href', imageUri);

        $(`.js-product-${image[0]}`, bigImg).attr('src', imageUri);
    });

    (function () {
        let outline = $('.js-outline-slider-wrap');
        let inline = $('.js-inline-slider-wrap');
        let clickEvent = new MouseEvent("click", {
            "view": window,
            "bubbles": true,
            "cancelable": false
        });

        setTimeout(function () {
            let outBox = outline.find('.js-marked');
            let inBox = outline.find('.js-marked');

            if (outBox.length && inBox.length) {
                outBox[0].click();
                inBox[0].click();

                document.querySelectorAll('.js-outline-slider-wrap .js-marked')[0].click(); //костыль для мобилок
                document.querySelectorAll('.js-inline-slider-wrap .js-marked')[0].click(); //костыль для мобилок
            }

        }, 0);

    }());

    $('.card-product__more').on('click', function () {
        $( "#services" ).trigger( "click" );
    })
});