$( function () {
    let targets = $( '[rel~=tooltip]' ),
        target  = false,
        tooltip = false,
        title   = false;

    targets.bind( 'mouseenter', function () {//mouseenter
        target  = $( this );
        tip     = target.attr( 'title' );
        tooltip = $( '<div class="tooltip"></div>' );

        if ( !tip || tip === '' )
            return false;

        target.removeAttr( 'title' );
        tooltip.css( 'opacity', 0 )
            .html( tip )
            .appendTo( 'body' );

        let init_tooltip = function () {
            let maxWidth = ($( window ).width() / 1.5) > 340 ? 360 : $( window ).width() / 1.5;
            if ( $( window ).width() < tooltip.outerWidth() * 1.5 ) {
                tooltip.css( 'max-width',  maxWidth);
            } else {
                tooltip.css('max-width', 360);
            }

            let pos_left = target.offset().left + ( target.outerWidth() / 2 ) - ( tooltip.outerWidth() / 2 ),
                pos_top  = target.offset().top - tooltip.outerHeight() - 20;

            if ( pos_left < 0 ) {
                pos_left = target.offset().left + target.outerWidth() / 2 - 20;
                tooltip.addClass( 'left' );
            } else {
                tooltip.removeClass('left');
            }

            if ( pos_left + tooltip.outerWidth() > $( window ).width() ) {
                pos_left = target.offset().left - tooltip.outerWidth() + target.outerWidth() / 2 + 20;
                tooltip.addClass( 'right' );
            } else {
                tooltip.removeClass('right');
            }

            if ( pos_top < 0 ) {
                let pos_top  = target.offset().top + target.outerHeight();
                tooltip.addClass( 'top' );
            } else {
                tooltip.removeClass( 'top' );
            }

            tooltip.css( { left: pos_left, top: pos_top } )
                .animate( { top: '+=10', opacity: 1 }, 50 );
        };

        init_tooltip();
        $( window ).resize( init_tooltip );

        let remove_tooltip = function () {
            tooltip.animate( { top: '-=10', opacity: 0 }, 50, function () {
                $( this ).remove();
            });

            target.attr( 'title', tip );
        };

        target.bind( 'mouseleave', remove_tooltip );
        tooltip.bind( 'click', remove_tooltip );
    });
});