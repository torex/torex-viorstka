var dropdown_next = {
	init: function() {
		var $obj = $('.js-dropdown');
		// $(window).on('resize', function(){
		// 	if($(window).innerWidth() >= 768){
		// 		$obj.each(function(index, el) {
		// 			var $el = $(el);
		// 			if ($el.hasClass('active')) {
		// 				$el.removeClass('active');
		// 				$el.next().show();
		// 			}
		// 			else{
		// 				$el.next().show();
		// 			}

		// 		});
		// 	}
		// 	else{
		// 		$obj.each(function(index, el) {
		// 			var $el = $(el);
		// 			$el.next().hide();
		// 		});
		// 	}
		// });
		if ($obj.length) {
			$obj.on('click', function(e) {
				if($obj.hasClass('js-only-mobile') && !($(window).innerWidth() < 768)) return;
				e.preventDefault();
				var $this =  $(this),
					$parent = $this.closest('.js-parent'),
					$all = $parent.length? $parent.find($obj) : $this;
					if ($parent.length) {
						$all.next().slideUp();
					}
				if ($this.hasClass('active')) {
					$all.removeClass('active');
					$this.next().slideUp();
				} else {
					$all.removeClass('active');
					$this.addClass('active');
					$this.next().slideDown();
				}
				
			});
			$('.door-card__header.js-only-mobile.js-dropdown').first().trigger('click');
		}
	}
}.init();