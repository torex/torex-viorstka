$body.on('click', '.js-scroll-top', function (event) {
    event.preventDefault();
    $body.add($('html')).stop().animate({ scrollTop: 0 }, 500);
    $body.add($('.js-tab-link')).stop().animate({ scrollTop: 0 }, 500); //custom
    $('.js-tab-link')[0].click();
});

$(window).scroll(function (event) {
    if ($(window).scrollTop() > 300) {
        $('.js-scroll-top').show();
        $('.js-scroll-menu').show();//custom
    } else {
        $('.js-scroll-top').hide();
        $('.js-scroll-menu').hide();//custom
    }
});

//custom block
$('body').on('click', '.js-scroll-menu', function (event) {
    event.preventDefault();
    let btn = $('.js-submenu');
    $('html, body').animate({scrollTop: btn.offset().top}, 500);
});
$(window).scroll(function () {
    if ($(window).scrollTop() > 600) {
        $('.js-scroll-menu').show();//custom
    } else {
        $('.js-scroll-menu').hide();//custom
    }
});
//custom block end