var tabs = {
	init: function() {
	    var $tabs = $('.js-tabs');
	    if ($tabs.length) {
	        $tabs.each(function(){
	            var $tab = $(this),
	            $tab_links = $tab.find('.js-tab-link');
	            $('body').on('click', '.js-tab-link', function(e) {
	                e.preventDefault();
	                var $tab_link = $(this);
	                $tab_links.removeClass('active');
	                $tab_link.addClass('active');
	                $tab.find('.js-tab-block').removeClass('visible');
	                $tab.find('.js-tab-block[data-tab="'+ $tab_link.attr('data-tab') +'"]').addClass('visible');
					$('.tab-block_inside-container').css('min-height', $tab.find('.js-tab-block_inside.visible').height());
	            }).first().trigger('click');		            
	        });
	    }
	}
}.init();

var tabsInside = {
	init: function() {
		var $tabs = $('.js-tabs_inside');
		if ($tabs.length) {
			$tabs.each(function() {
				let $tab = $(this);
				let $tab_links = $tab.find('.js-tab-link_inside');
				$('body').on('click', '.js-tab-link_inside', function(e) {
					e.preventDefault();
					let $tab_link = $(this);
					$tab_links.removeClass('active');
					$tab_link.addClass('active');
					$tab.find('.js-tab-block_inside').removeClass('visible');
					$tab.find('.js-tab-block_inside[data-tab="'+ $tab_link.attr('data-tab') +'"]').addClass('visible');
					$('.tab-block_inside-container').css('min-height', $tab.find('.js-tab-block_inside[data-tab="'+ $tab_link.attr('data-tab') +'"]').height());
				}).first().trigger('click');
			});
		}
	}
}.init();