console.log("map connected");

let myMap;
let balloonTemplate;
let pinData;
let myCollection;
let isActive;

// Шаблон балуна.
balloonTemplate = '<div class="sale-points__popover">' +
        '<a class="sale-points__close" href="#">Закрыть <span class="cross">&times;</a>' +
        '<div class="sale-points__arrow"></div>' +
        '<img class="sale-points__logo" src="images/logo-for-map.png" alt="">' +
        '<div class="sale-points__popover-inner">' +
            '$[[options.contentLayout]]' +
        '</div>' +
    '</div>';

// Генерация списка городов для меток на карте
function createLocationMenu(data) {
    let container = $('.cartographer__list-places');

    container.empty();
    container.css('display', 'block');

    data.forEach(function (menuItem, index) {
        let template;
        let phones = '';

        if (menuItem['phone'].length) {

            menuItem['phone'].forEach(function (phone) {

                if (!phone) return;

                phones += `<span class='phone'>${phone}</span>`;
            });
        }

        template = `<label for="cartographer__radio-${index}" id="marker-${index}" class="cartographer__menu-item" data-coord="${menuItem['coords']}">
                <input type="radio" name="cartographer__sale-point" id="cartographer__radio-${index}" class="cartographer__input" value="">
                <div class="cartographer__map-card">
                    <div class="cartographer__manager"><a href="#"><b>${menuItem['manager']}</b></a></div>
                    <div class="cartographer__address">${menuItem['address']}</div>
                    <div class="cartographer__phone-number">${phones}</div>
                    <div class="cartographer__working-hours">${menuItem['time']}</div>
                    <div class="cartographer__site"><a href="//${menuItem['site']}" target="_blank">${menuItem['site']}</a></div>
                    <div class="cartographer__email"><a href="mailto:${menuItem['email']}">${menuItem['email']}</a></div>
                </div>
            </label>`;

        container.append(template);
    });

    $('.cartographer__map-card').on('click', function () {
        let coord = $(this).parent().data('coord').split(',');
        let id = $(this).parent().attr('id');

        myMap.balloon.close();

        go_point(id, coord);
    });
}

function go_point(id) {
    myCollection.each(function (item) {

        if (item.properties.get('iden') === id) {
            let coordinates = item.geometry.getCoordinates();
            let marker = item.getOverlaySync().getLayoutSync().getElement();
            myMap.status

            myMap.setCenter(coordinates, myMap.getZoom(), {duration: 300});

            setTimeout(function () {
                $(marker).find('.sale-points__square').addClass('interested__always');
                item.balloon.open();
            }, 300);
        }
    });
}

function checkPin(pinCoord) {
    let container = $('.cartographer__list-places');
    let itemsArray = container.find('.cartographer__menu-item');

    itemsArray.each(function() {
        let pinner = $(this).data('coord');

        if (pinner === pinCoord) {
            $(this).find('.cartographer__input').prop('checked', true);
        }
    });
}

function addPin(arrayData) {
    let phones;
    let myPlacemark = '';
    let myBalloonLayout;
    let myBalloonContentLayout;
    // Шаблон метки.
    let myIcon;

    // Создание макета балуна.
    myBalloonLayout = ymaps.templateLayoutFactory.createClass(balloonTemplate, {
        // Строит экземпляр макета на основе шаблона
        // и добавляет его в родительский HTML-элемент.
        build: function () {
            this.constructor.superclass.build.call(this);
            this._$element = $('.sale-points__popover', this.getParentElement());
            this.applyElementOffset();
            this._$element.find('.sale-points__close')
                .on('click', $.proxy(this.onCloseClick, this));
        },

        // Удаляет содержимое макета из DOM.
        clear: function () {
            this._$element.find('sale-points__close').off('click');
            this.constructor.superclass.clear.call(this);
            //$('.sale-points__square').removeClass('interested__always');
        },

        // Метод будет вызван системой шаблонов АПИ при изменении размеров вложенного макета.
        onSublayoutSizeChange: function () {
            myBalloonLayout.superclass.onSublayoutSizeChange.apply(this, arguments);

            if(!this._isElement(this._$element)) {
                return;
            }
            this.applyElementOffset();
            this.events.fire('shapechange');
        },

        // Сдвигаем балун, чтобы "хвостик" указывал на точку привязки.
        applyElementOffset: function () {
            this._$element.css({
                left: -(this._$element[0].offsetWidth / 2),
                top: -(this._$element[0].offsetHeight + this._$element.find('.sale-points__arrow')[0].offsetHeight)
            });
        },

        // Закрывает балун при клике на крестик, кидая событие "userclose" на макете.
        onCloseClick: function (e) {
            e.preventDefault();

            this.events.fire('userclose');
        },

        // Используется для автопозиционирования (balloonAutoPan).
        getShape: function () {
            if(!this._isElement(this._$element)) {
                return myBalloonLayout.superclass.getShape.call(this);
            }

            let position = this._$element.position();

            return new ymaps.shape.Rectangle(new ymaps.geometry.pixel.Rectangle([
                [position.left, position.top], [
                    position.left + this._$element[0].offsetWidth,
                    position.top + this._$element[0].offsetHeight + this._$element.find('.sale-points__arrow')[0].offsetHeight
                ]
            ]));
        },

        // Проверяем наличие элемента (в ИЕ и Опере его еще может не быть).
        _isElement: function (element) {
            return element && element[0] && element.find('.sale-points__arrow')[0];
        }
    });

    if (arrayData.length === 0) return;

    myCollection.removeAll();
    isActive = false;

    createLocationMenu(arrayData);

    arrayData.forEach(function (pin, index) {
        phones = '';

        if (pin.phone.length) {
            pin['phone'].forEach(function (phone) {
                phones += `<span class='phone'>${phone}</span>`;
            });
        }

        myIcon = ymaps.templateLayoutFactory.createClass(`<div id="marker-${index}" class="sale-points__square" data-marker="${pin.coords}"><div class="orange-point"></div></div>`);

        // Создание вложенного макета содержимого балуна.
        myBalloonContentLayout = ymaps.templateLayoutFactory.createClass(
            `<div class="sale-points__content" data-coords="${pin.coords}">` +
                `<p class="sale-points__paragraph paragraph-title"><b>${pin.manager}</b></p>` +
                `<p class="sale-points__paragraph address">${pin.address}</p>` +
                `<p class="sale-points__paragraph phone-number">${phones}</p>` +
                `<p class="sale-points__paragraph worktime">${pin.time}</p>` +
                `<p class="sale-points__paragraph site"><a href="//${pin.site}" target="_blank">${pin.site}</a></p>` +
                `<p class="sale-points__paragraph email"><a href="mailto:${pin.email}">${pin.email}</a></p>` +
            `</div>`
        );

        myPlacemark = new ymaps.Placemark(pin.coords,
            {},
            {
                // Опции.
                // Макет иконки.
                iconLayout: myIcon,
                // Смещение левого верхнего угла иконки относительно её "ножки" (точки привязки).
                iconLayoutOffset: [-30, 15],
                // Описываем фигуру активной области "Круг".
                iconShape: {
                    type: 'Circle',
                    // Круг описывается в виде центра и радиуса
                    coordinates: [15, 15],
                    radius: 15
                },
                pointOverlay: 'html#placemark',
                // Отключение скрытия метки и позиционирование балуна относительно ее
                hideIconOnBalloonOpen: false,
                balloonMinHeight: 260,
                balloonMaxHeight: 600,
                balloonShadow: false,
                balloonLayout: myBalloonLayout,
                balloonContentLayout: myBalloonContentLayout,
                balloonPanelMaxMapArea: 0,
                balloonOffset: [15, -15]
            });

        myPlacemark.events
            .remove(['mouseenter', 'mouseleave'])
            .add('mouseenter', function (event) {
                let _this = event.get('target').getOverlaySync().getLayoutSync().getElement();
                let pinner = _this.children[0];

                $(pinner).addClass('interested');
            })
            .add('mouseleave', function (event) {
                let _this = event.get('target').getOverlaySync().getLayoutSync().getElement();
                let pinner = _this.children[0];

                $(pinner).removeClass('interested');
                event.get('target').options.set({iconOffset: [0, 0]});
            })
            .add('click', function (event) {
                let _this = event.get('target').getOverlaySync().getLayoutSync().getElement();
                let pinner = _this.children[0];

                $('.sale-points__square').removeClass('interested__always');
                $(pinner).addClass('interested__always');
                checkPin($(pinner).data('marker'));
                event.get('target').options.set({iconOffset: [0, 0]});
            })
            /*.add('balloonclose', function(event) {
                let obj = event.get('target');

                if (!obj.hasOwnProperty('getOverlaySync')) return;

                let _this = event.get('target').getOverlaySync().getLayoutSync().getElement();
                let pinner = _this.children[0];

                $(pinner).removeClass('interested__always');
            });*/
        myPlacemark.properties.set('iden', `marker-${index}`);
        myCollection.add(myPlacemark);
    });

    myMap.geoObjects.events.add('balloonclose', function (event) {
        let _this = event.get('target').getOverlaySync().getLayoutSync().getElement();
        let pinner = _this.children[0];

        $(pinner).removeClass('interested__always');
    });

    myMap.geoObjects.add(myCollection);
    myMap.setBounds(myCollection.getBounds(),{
        checkZoomRange: true,
        zoomMargin: 10
    });
    myMap.behaviors.disable('scrollZoom');
}

function getPin() {
    let cursor = $('.multiselect__button').data('city');

    addPin(pinData[cursor]);
}

function getData() {

    $.ajax({
        type: 'get',
        url: './data/city-points.json',
        xhrFields: {
            withCredentials: false
        },
        success: function (data) {
            pinData = data;
            getPin();
        },
        error: function (xhr, status, error) {
            console.log(error);
        }
    });
}

function init() {
    let coordinates = [55.751574, 37.573856];

    myCollection = new ymaps.GeoObjectCollection();

    // Скрытие прелоадера
    $('.sale-points__loader').css('display', 'none');

    // Настройка инициализации отображения карты
    myMap = new ymaps.Map('map',
        {
            center: coordinates,
            zoom: 11,
            controls: []
        },
        {
            suppressMapOpenBlock: true
        });

    getData();
}

$('.multiselect__menu').on('change', function () {
    let data = $('.interselect:checked', '.multiselect__menu').parent().find('.checkbox__input').val();

    $(this).data('city', data);
    addPin(pinData[data]);
});

if (typeof ymaps !== 'undefined') {
    ymaps.ready(init);
}

