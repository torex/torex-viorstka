(() => {
    const media = {
        lg: 1259,
        md: 991,
        sm: 767,
        xs: 575,
        min: 0
    };
    $body = $('body');

    // Функция и ниже пример для работы с адаптивом через matchMedia
    function match_Media(breackpointMax, breackpointMin) {
        breackpointMax = breackpointMax || 'max';
        breackpointMin = breackpointMin || 'min';
        return (breackpointMax == 'max' ? window.matchMedia("(min-width: " + (media[breackpointMin] + 1) + "px)").matches : window.matchMedia("(max-width: " + media[breackpointMax] + "px) and (min-width: " + (media[breackpointMin] + 1) + "px)").matches);
    }

    var last_breakpoint;
    $(window).resize(function (event) {
        var prev_objectKey = 'max';
        Object.keys(media).map(function (objectKey, index) {
            if (match_Media(prev_objectKey, objectKey) && last_breakpoint != objectKey) {
                prev_objectKey = prev_objectKey == "max" ? 'xl' : prev_objectKey;
                console.log("это " + prev_objectKey);
                last_breakpoint = objectKey;
            }
            prev_objectKey = objectKey;
        });
    }).trigger('resize');
})();