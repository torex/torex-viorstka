let doorParams = [
	{
		"place":"cottage",
		"side":"out",
		"color":"color01",
		"texture":"texture01",
		"image":"door--place_cottage--side_out--color_color01--texture_texture01.png",
	},
	{
		"place":"cottage",
		"side":"in",
		"color":"color01",
		"texture":"texture01",
		"image":"door--place_cottage--side_in--color_color01--texture_texture01.png",
	},
	{
		"place":"cottage",
		"side":"out",
		"color":"color01",
		"texture":"texture02",
		"image":"door--place_cottage--side_out--color_color01--texture_texture02.png",
	},
	{
		"place":"cottage",
		"side":"in",
		"color":"color01",
		"texture":"texture02",
		"image":"door--place_cottage--side_in--color_color01--texture_texture02.png",
	},
	{
		"place":"cottage",
		"side":"out",
		"color":"color02",
		"texture":"texture01",
		"image":"door--place_cottage--side_out--color_color02--texture_texture01.png",
	},
	{
		"place":"cottage",
		"side":"in",
		"color":"color02",
		"texture":"texture01",
		"image":"door--place_cottage--side_in--color_color02--texture_texture01.png",
	},
	{
		"place":"cottage",
		"side":"out",
		"color":"color02",
		"texture":"texture02",
		"image":"door--place_cottage--side_out--color_color02--texture_texture02.png",
	},
	{
		"place":"cottage",
		"side":"in",
		"color":"color02",
		"texture":"texture02",
		"image":"door--place_cottage--side_in--color_color02--texture_texture02.png",
	},
	{
		"place":"flat",
		"side":"out",
		"color":"color01",
		"texture":"texture01",
		"image":"door--place_flat--side_out--color_color01--texture_texture01.png",
	},
	{
		"place":"flat",
		"side":"in",
		"color":"color01",
		"texture":"texture01",
		"image":"door--place_flat--side_in--color_color01--texture_texture01.png",
	},
	{
		"place":"flat",
		"side":"out",
		"color":"color01",
		"texture":"texture02",
		"image":"door--place_flat--side_out--color_color01--texture_texture02.png",
	},
	{
		"place":"flat",
		"side":"in",
		"color":"color01",
		"texture":"texture02",
		"image":"door--place_flat--side_in--color_color01--texture_texture02.png",
	},
	{
		"place":"flat",
		"side":"out",
		"color":"color02",
		"texture":"texture01",
		"image":"door--place_flat--side_out--color_color02--texture_texture01.png",
	},
	{
		"place":"flat",
		"side":"in",
		"color":"color02",
		"texture":"texture01",
		"image":"door--place_flat--side_in--color_color02--texture_texture01.png",
	},
	{
		"place":"flat",
		"side":"out",
		"color":"color02",
		"texture":"texture02",
		"image":"door--place_flat--side_out--color_color02--texture_texture02.png",
	},
	{
		"place":"flat",
		"side":"in",
		"color":"color02",
		"texture":"texture02",
		"image":"door--place_flat--side_in--color_color02--texture_texture02.png",
	},

];
let imgPath = "images/configurator-doors/";
// let imgSrcPattern = "door--place_"+selectedPlace+"--side_"+selectedSide+"--color_"+selectedColor+"--texture_"+selectedTexture+".png";
let Events = {

	bindEvents: function () {
		$('.header_menu__toggle').off('click', this.mainMenuToggle).on('click', this.mainMenuToggle);
		$('.city-list__country').off('click', this.selectCountry).on('click', this.selectCountry);
		$('.city-list__more span').off('click', this.showAllCities).on('click', this.showAllCities);
		$('.company-stats__more-toggle').off('click', this.companyStatsToggle).on('click', this.companyStatsToggle);
	//configurator on frontpage
		$('#configurator_cottage-flat .configurator__tab').off('click', this.configuratorCottFlatTabAndImageToggle).on('click', this.configuratorCottFlatTabAndImageToggle);
		$('#configurator_out-in .configurator__tab').off('click', this.configuratorOutInTabAndImageToggle).on('click', this.configuratorOutInTabAndImageToggle);
		$('.configurator__color-item').off('click', this.configuratorSetColor).on('click', this.configuratorSetColor);
		$('.configurator__texture-item').off('click', this.configuratorSetTexture).on('click', this.configuratorSetTexture);
			
	//end configurator
		$('.company-stats__icon').off('click', this.showStatsTooltip).on('click', this.showStatsTooltip);
		$('.company-stats__tooltip-close').off('click', this.hideStatsTooltip1).on('click', this.hideStatsTooltip1);
		$(document).off('click', this.hideStatsTooltip2).on('click', this.hideStatsTooltip2);
		// this.doorParams();





	},//bindEvents

	mainMenuToggle: function(){
		$(".header_menu").toggle(80);
		$(this).toggleClass("toggle-active");
		$("body").toggleClass("menu-active");
	},
	//select city popup
	selectCountry: function(){
		let country = $(this).attr("data-content");
		$(this).parents(".city-list__content").find(".city-list__city-list").removeClass("city-list_active");
		$(".city-list__city-wrap").find("#" + country).addClass("city-list_active");

	},
	showAllCities: function(){
		$("#all-cities").animate({opacity: 'toggle'}, 'fast');
		var text = $(this).text();
		$(this).text(
		text == "Все города" ? "Скрыть список" : "Все города");
	},


	

	//configurator on frontpage
	configuratorCottFlatTabAndImageToggle: function() {
		console.log("configuratorCottFlatTabAndImageToggle calld");
		if ($(this).hasClass("tab-active")) {
			return false;
		}
		else {
			let selectedPlace = $(this).attr("data-place");
			let defaultOutImgSrc = $(this).attr("data-image-out");
			let defaultInImgSrc = $(this).attr("data-image-in");
			$(this).parent("#configurator_cottage-flat").find(".configurator__tab").removeClass("tab-active").addClass("tab-inactive");
			$(this).toggleClass("tab-active").removeClass("tab-inactive");
			//change main image
			$("#image-outside").attr("src", defaultOutImgSrc);
			$("#image-inside").attr("src", defaultInImgSrc);
			console.log(selectedPlace);

		}
	},
	configuratorOutInTabAndImageToggle: function() {
		console.log("configuratorOutInTabAndImageToggle calld");
		if ($(this).hasClass("tab-active")) {
			return false;
		}
		else {
			let selectedSide = $(this).attr("data-side");
			let isToggle = $(this).closest('.js-toggle-content');
			let toggleMe = isToggle.find('.js-toggle-me');

			$(this).parent("#configurator_out-in").find(".configurator__tab").removeClass("tab-active").addClass("tab-inactive");
			$(this).toggleClass("tab-active").removeClass("tab-inactive");
			//swap images
			$(".configurator__image").toggleClass("image-active");
			//swap content
			let confContent = $(this).attr("data-content");
			$(".door-configurator__content_left").find(".configurator_opts").removeClass("content-active");
			$(".door-configurator__content_left").find("#" + confContent).addClass("content-active");

			$.each(toggleMe, function () {
				$(this).removeClass('toggle-me');
			});

			isToggle.find(`.js-toggle-me[data-toggle="${$(this).data('find')}"]`).addClass('toggle-me');

			console.log(selectedSide);
		}
	},
	configuratorSetColor: function() {
		let imgOutSrc = $("#image-outside").attr("src").split("/door--")[1];
		let imgInSrc = $("#image-inside").attr("src").split("/door--")[1];
		let selectedColor = $(this).attr("data-color");
		$(this).parents('.configurator__color-wrap').find('.configurator__color-item').removeClass('color-active');
		$(this).toggleClass('color-active');

		console.log("configuratorSetColor called");
		console.log(selectedColor);
	},
	configuratorSetTexture: function() {
		let selectedTexture = $(this).attr("data-texture");
		$(this).parents('.configurator__texture-wrap').find('.configurator__texture-item').removeClass('texture-active');
		$(this).toggleClass('texture-active');

		console.log("configuratorSetTexture called");
		console.log(selectedTexture);
	},

	//end configurator
	showStatsTooltip: function(){
		$(this).parents(".company-stats__item").siblings().removeClass("tooltip-active");
		$(this).parents(".company-stats__item").toggleClass("tooltip-active");
		
	},
	hideStatsTooltip1: function(){
		$(this).parents(".company-stats__item").removeClass("tooltip-active");
	},

	hideStatsTooltip2: function(e){
		if ($(e.target).closest(".tooltip-active").length === 0) {
			$(".company-stats__item").removeClass("tooltip-active");;
		}
	},

	companyStatsToggle: function(){
		$(this).parent(".company-stats__more").toggleClass("active");

	},





};//Events