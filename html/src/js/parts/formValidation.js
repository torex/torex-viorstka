$(document).ready(function() {
	console.log("form validation enabled but partially");
$('.js-validation').each(function(){
	$(this).validate({
		onfocusout: false,
		focusInvalid: false,
		errorClass: "incorrect",
		errorPlacement: function(error, element) {},
		lang: 'ru',
		validClass: 'is-valid',
		ignore: ":hidden",
		submitHandler: function(form) {
			var validator = this;
			let name_form = $(form).attr('data-nameForm');
			$(form).ajaxSubmit( {
				success: function(response) {
					if (name_form == "rating-form") {
						$('body').prepend('<div class="success_form"><div class="success_form__cover"></div><div class="success_form__text">Спасибо!<br> Ваша оценка успешно отправлена.<br><div class="btn form_success_btn" data-name="' + name_form + '"><span>Хорошо</div></div></div></div>');
						$(form).find('input, textarea, button, select').attr('disabled', true);
					} else {
						$('body').prepend('<div class="success_form"><div class="success_form__cover"></div><div class="success_form__text">Спасибо!<br> Ваша заявка успешно отправлена.<br> Мы скоро свяжемся с вами.<br><div class="btn form_success_btn" data-name="' + name_form + '"><span>Хорошо</div></div></div></div>');
						validator.resetForm();
					}

				}
			});
			return false;
		},
	});
});
	

		// $.validator.addMethod("phoneRu", function(phone_number, element) {
		// 	return this.optional(element) || phone_number.length > 9 && (/(\+7|8)\s\(\d{3}\)\s\d{3}-\d{4}/g).test(phone_number);
		// }, 
		// $.validator.messages.phoneRu || "Пожалуйста введите корректный номер телефона");

		// $(document).on('click','.form_success_btn', function () {
		// 	var name_form = $(this).attr('data-name');
		// 	$('[data-nameForm="'+name_form+'"]').find('#name').val('');
		// 	$('[data-nameForm="'+name_form+'"]').find('#phone').val('');
		// 	$('.success_form').remove();
		// });
});