let View = {
	initView: function () {
		//map block, shop photos popup
		$('[data-fancybox="preview"]').fancybox({
			loop: true
		});



	},
	initWidgets: function() {
		// this.initHyphenate();
		// this.initTruncateText();
	},
	initSliders: function() {
		//top slider on frontpage
		// $(window).on("load resize", (function topSlider() {
		let topSlider = $(".top-slider__slider");
		let lazyImages = topSlider.find('.slide-image');
		let lazyCounter = 0;
		// Initialize
		topSlider.on("init", function(slick){
			slick = $(slick.currentTarget);
			$(".top-slider").addClass("inited");
		});
		topSlider.on("beforeChange", function(event, slick) {
			slick = $(slick.$slider);
			$(".slick-arrow").addClass("bounce-arrow");
		});
		topSlider.on("afterChange", function(event, slick) {
			slick = $(slick.$slider);
			$(".slick-arrow").removeClass("bounce-arrow");
		});
		topSlider.on("lazyLoaded", function(event, slick, image, imageSource) {
			lazyCounter++;
			if (lazyCounter === lazyImages.length){
				lazyImages.addClass('show');
			}
		});
		topSlider.slick({
			autoplay: true,
			autoplaySpeed:10000,
			lazyLoad:"progressive",
			speed:600,
			arrows:true,
			dots:true,
			dotsClass:"top-slider__dots slick-dots",
			cssEase:"cubic-bezier(0.87, 0.03, 0.41, 0.9)",
			responsive: [
			{
				breakpoint: 1260,
				settings: {
					dots: false,
				}
			},
			{
				breakpoint: 740,
				settings: "unslick",
			}
			]
		});
	// }));//load resize


  $(window).on('load resize orientationchange', function() {
        $('[data-slider="mob-only"]').each(function(){
            var $carousel = $(this);
            if ($(window).width() > 739) {
                if ($carousel.hasClass('slick-initialized')) {
                    $carousel.slick('unslick');
                }
            }
            else{
                if (!$carousel.hasClass('slick-initialized')) {
                    $carousel.slick({
                        arrows: false,
						dots: true,
						dotsClass: "mobile-only-dots slick-dots",
						infinite: true,
						speed: 300,
						slidesToShow: 1,
						slidesToScroll: 1,
                    });
                }
            }
        });
    });

		$('[data-slider="slides-3-1"]').slick({
			dots: false,
			infinite: true,
			speed: 300,
			slidesToShow: 3,
			slidesToScroll: 3,
			responsive: [{
			breakpoint: 1260,
				settings: {
					dots: false,
					infinite: true,
					speed: 300,
					slidesToShow: 1,
					slidesToScroll: 1
				}
			},
			{
			breakpoint: 740,
				settings: {
					dots: true,
					arrows: false,
					infinite: true,
					speed: 300,
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}]
		});

		//в тз написано убрать, но пусть будет пока
		// $('.company-about__slider').slick({
		// 	dots: true,
		// 	dotsClass: "company-about__dots slick-dots",
		// 	arrows: false,
		// 	infinite: true,
		// 	speed: 300,
		// 	slidesToShow: 1,
		// 	slidesToScroll: 1,
		// 	responsive: [{
		// 		breakpoint: 1260,
		// 		settings: "unslick",
		// 	},]
		// });

		$('[data-slider="configurator__slider"]').slick({
			dots: false,
			infinite: true,
			speed: 300,
			slidesToShow: 5,
			slidesToScroll: 5,
			responsive: [{
				breakpoint: 1260,
				settings: {
					slidesToShow: 8,
					slidesToScroll: 8
				}
			},]
		});

	},//sliders

	// initHyphenate: function () {
	// 	if ($("[data-hyphenate='true']").length > 0) {
	// 		$("[data-hyphenate='true']").each(function () {
	// 			$(this).hyphenate();
	// 		});
	// 	}
	// },
	// initTruncateText: function () {
	// 	if ($("[data-truncate]").length > 0) {
	// 		$("[data-truncate]").each(function () {
	// 			let text = $(this).html();
	// 			let maxLength = $(this).data('truncate');
	// 			if (maxLength > 1) {
	// 				$(this).html(shorten(text, maxLength))
	// 			}
	// 		});
	// 	}
	// },
//


}//let View 
