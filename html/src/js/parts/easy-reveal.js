(function() {
    'use strict';

    /* PARAMETERS  */
    let offset   = 85; //int in %

    /* D'ONT TOUCH ANY FURTHER IF YOU DON'T KNOW WHAT YOU ARE DOING */
    let $targets = $('[data-easy-reveal]');

    let windowHeight = $(window).height(),
        offsetHeight = windowHeight * offset / 100,
        docHeight    = $(document).height();

    function reveal(){
        $targets.each(function () {
            let targetTop    = $(this).offset().top;
            let windowScroll = $(window).scrollTop();
            let animation    = $(this).data('easy-reveal');
            //default animation
            if (!animation) { animation = 'fade-in-up'; }

            // lunch animation if scroll is further the target + offset  OR at the end of the page
            if ( targetTop < ( windowScroll + offsetHeight) || (windowScroll + windowHeight) === docHeight) {
                $(this).addClass(animation);
            }
        });
    }

    function safeAppear() {
        let windowScroll = $(window).scrollTop();
        if (windowScroll > 1000) {
            $('[data-easy-reveal]').addClass("fade-in");
            console.log("safety!");
        }
    }

    // lunch animation on scroll and document ready
    $(document).ready(reveal);
    $(window).scroll(reveal);
    $(window).resize(safeAppear);

}());

function customReveal() {
    'use strict';

    /* PARAMETERS  */
    let offset   = 85; //int in %

    /* D'ONT TOUCH ANY FURTHER IF YOU DON'T KNOW WHAT YOU ARE DOING */
    let $targets = $('[data-custom-reveal]');
    let docHeight    = $(document).height();
    let windowHeight = $(window).height();
    let offsetHeight = windowHeight * offset / 100;
    let checkWindowScroll = $(window).scrollTop();

    function reveal() {
        $targets.each(function(){
            let targetTop    = $(this).offset().top;
            let windowScroll = $(window).scrollTop();
            let animation    = $(this).data('custom-reveal');
            //default animation
            if (!animation) { animation = 'fade-in-up'; }

            // lunch animation if scroll is further the target + offset  OR at the end of the page
            if ( targetTop < ( windowScroll + offsetHeight) || (windowScroll + windowHeight) === docHeight) {
                $(this).addClass(animation);
            }
        });
    }

    if (checkWindowScroll > 500) {
        setTimeout(function () {
            reveal();
        }, 1000);
    }

    // lunch animation on scroll and document ready
    $(window).scroll(reveal);
}

$('.js-init-reveal').on('click', function () {
    customReveal();
});
