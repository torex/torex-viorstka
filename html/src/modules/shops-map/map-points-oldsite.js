console.log("map connected (oldsite)");
// function doimg () {
// 	$('.balloon-img-wrap').magnificPopup({
// 		delegate: 'a',
// 		type: 'image',
// 		tLoading: 'Loading image #%curr%...',
// 		mainClass: 'mfp-img-mobile',
// 		gallery: {
// 			enabled: true,
// 			navigateByImgClick: true,
// 			preload: [0,1]
// 		},
// 		image: {
// 			tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
// 			titleSrc: function(item) {
// 				return item.el.attr('title') + '<small>TOREX</small>';
// 			}
// 		}
// 	});
// }
// function doimg_scheme () {
// 	$('.location-help-scheme-wrapper').magnificPopup({
// 		delegate: 'a',
// 		type: 'image',
// 		tLoading: 'Loading image #%curr%...',
// 		mainClass: 'mfp-img-mobile',
// 		gallery: {
// 			enabled: false
// 		},
// 		image: {
// 			tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
// 			titleSrc: function(item) {
// 				return '<small>'+item.el.attr('title')+'</small>';
// 			}
// 		}
// 	});
// }
var Network = {
	predefined_stations: JSON.parse('[]'),
	storesMap: null,
	placeMarkers: {},
	currentPlaceMarkersState: {},
	initNetworkMap: function(){
		if ($("#ymaps-script").length==0) {
			$('body').append('<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU&onload=Network.setNetworkMap" id="ymaps-script" type="text/javascript">');
		} else {
			Network.setNetworkMap();
		}
	},
	setNetworkMap: function(){
		Network.storesMap = new ymaps.Map('shops-map', {center: [55.65936277309159,38.00342888121918],zoom: 9});
		Network.addSalepoints();
	},
	addSalepoints: function(){
		var temp;
		for(var i in sale_points) {
			Network.placeMarkers[sale_points[i].id] = new ymaps.Placemark(sale_points[i].coords.split(","),{
				balloonContent: sale_points[i].balloon_text,
				id: sale_points[i].id
			}, {
				balloonShadow: true,
				iconLayout: 'default#image',
				iconImageHref: sale_points[i].icon,
				iconImageSize: [32, 37]
			});
			Network.placeMarkers[sale_points[i].id].events.add('balloonclose', function(e){
				var id = e.originalEvent.currentTarget.properties._data.id;
				$("[data-salepoint-item='"+id+"']").removeClass('active');
			});
			Network.placeMarkers[sale_points[i].id].events.add('balloonopen', function(e){
				var id = e.originalEvent.currentTarget.properties._data.id;
				$("[data-salepoint-item].active").removeClass('active');
				$("[data-salepoint-item='"+id+"']").addClass('active');
				$(".balloon-wrapper [data-original]").lazyload({
					effect:"fadeIn",
					placeholder:"data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
				});
				$(window).trigger("ll-force-update");
				$('.js-promosys-network-map-tooltip').torexPromosysTooltip('network-map');
			});
			var metro_matched = false;
			for(var m in sale_points[i].subway) {
				if (Network.predefined_stations.indexOf(sale_points[i].subway[m]) != -1) {
					metro_matched = true;
				}
			}
			if (Network.predefined_stations.length == 0 || metro_matched) {
				Network.storesMap.geoObjects.add(Network.placeMarkers[sale_points[i].id]);
				Network.currentPlaceMarkersState[sale_points[i].id] = true;
			}
		}
	}
};
$(document).on("click", "[data-salepoint-item]", function(){
	var id = $(this).attr("data-salepoint-item");
	if (typeof Network.placeMarkers[id] != "undefined") {
		Network.placeMarkers[id].balloon.open();
	}
});