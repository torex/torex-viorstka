// // console.log("map connected");

// let myMap;
// let mapPlacemark;
// let pointerTemplate;
// let balloonTemplate;
// let myCollection;
// let objectManager;
// let placemarkCoordsArray;

// function getLocalData() {
//     placemarkCoordsArray = 
//     [
//         {
//             "id": "moscow-01",
//             "coords": [55.828010, 37.489392],
//             "address": "г. Москва, ул. Ленинградское шоссе, д. 25",
//             "phone": ["+7(985)773-74-07", "+7 (495) 773-74-07"],
//             "time": "Время работы с 10.00 до 20.00",
//             "site": "-",
//             "email": "leningradsky@torex99.ru"
//         } ,
//         {
//             "id": "moscow-02",
//             "coords": [55.843252, 37.488925],
//             "address": "г. Москва, ул. Кронштадтский бульвар, д. 9, стр. 4, павильон Б5",
//             "phone": "+7 (909) 995-07-77",
//             "time": "Время работы с 10.00 до 20.00",
//             "site": "-",
//             "email": "stroydvor@torex99.ru"
//         },
//         {
//             "id": "moscow-03",
//             "coords": [55.7164,37.6025],
//             "address": "г. Москва, ул. Мневники, д. 10, корп. 1",
//             "phone": "+7 (985) 211-31-26",
//             "time": "Время работы пн-сб с 10.00 до 20.00",
//             "site": "-",
//             "email": "mnevniki@torex99.ru"
//         }
//     ]
    
// }

// //main function!!
// function populateMap() {
//     getLocalData();

//     placemarkCoordsArray.map((item, index) => {
//       // console.log('coords: ' + item.coords + ' | id: ' + item.id + ' | address: ' + item.address);
//     });

// }

// function addPlacemarks() {
//     getLocalData();
    
//     placemarkCoordsArray.forEach((item, value) => {

//         // create pointer & balloon layout
//         pointerTemplate = ymaps.templateLayoutFactory.createClass(
//             '<div class="shops-map__map-point">$[properties.iconContent]</div>'
//         );
        
//         let balloonTemplate = 
//             '<div class="shops-map__popover">' +
//                 '<div class="shops-map__arrow"></div>' +
//                 '<div class="shops-map__balloon-content">' +
//                     '<p class="shops-map__balloon-item address">' +
//                     item.address +
//                     '</p>' +
//                     '<p class="shops-map__balloon-item phone-number">' +
//                     item.phone +
//                     '</p>' +
//                     '<p class="shops-map__balloon-item worktime">' +
//                     item.time +
//                     '</p>' +
//                     '<p class="shops-map__balloon-item site"><a href="//' +
//                     item.site +
//                     '" target="_blank">' +
//                     item.site +
//                     '</a></p>' +
//                     '<p class="shops-map__balloon-item email"><a href="mailto:' +
//                     item.email + 
//                     '">' + 
//                     item.email + 
//                     '</a></p>' +
//                     '</div>'
//                 '</div>' +
//             '</div>';
//             // console.log(item.coords);

//         mapPlacemark = new ymaps.Placemark(item.coords,
//             {
//                 balloonContent: balloonTemplate,
//                 iconContent: "",
//             }, {
//                 iconLayout: 'default#imageWithContent',
//                 iconImageSize: [20, 20],
//                 iconImageOffset: [-8, -8],
//                 iconContentOffset: [-3, -2],
//                 iconContentLayout: pointerTemplate
//         });

//         myCollection.add(mapPlacemark);
//     });//map & each
//     myMap.geoObjects.add(myCollection);

// }//addPlacemarks



// function init() {
//     myCollection = new ymaps.GeoObjectCollection();
// 	// Creating the search & zoom controls
// 	let searchControl = new ymaps.control.SearchControl({
// 	    options: {
// 	        provider: 'yandex#search'
// 	    }
// 	});
//     let zoomControl = new ymaps.control.ZoomControl({
//         options: {
//             size: "small"
//         }
//     });

//     // map display
//     myMap = new ymaps.Map('shops-map',
//         {
//             center: [55.828010, 37.489392],
//             zoom: 12,
//             controls: [searchControl, zoomControl],
//         },
//         {
//             suppressMapOpenBlock: true
//     });
// 	 myMap.behaviors.disable('scrollZoom');

//     // hide preloader
//     $('.shops-map__loading').css('display', 'none');

//     addPlacemarks();

// }

// //check if map is present on page
// if (typeof ymaps !== 'undefined') {
//     ymaps.ready(init);
// }


if (typeof ymaps !== 'undefined') {
    ymaps.ready(init);
}

function init() {
    let coordinates = [55.931903, 37.511961];
    var location = ymaps.geolocation;
    var myMap = new ymaps.Map('shops-map', {
        center: coordinates,
        zoom: 10
    }),
    objectManager = new ymaps.ObjectManager(),
    data_points = {
        "type": "FeatureCollection",
        "features": [
            {"type": "Feature", "id": 0, "geometry": {"type": "Point", "coordinates": [55.831903, 37.411961]},
                "properties": {
                    // "balloonContentHeader": "Это balloonContentHeader",
                    "balloonContentBody": "<div class='shop-map__balloon-body'>Тут любой контент</dib>",
                    // "balloonContentFooter": "это balloonContentFooter",
                    "hintContent": "Это hintContent"
                },
                "options" : {
                    "iconLayout" : "default#image",
                    "iconImageHref" : "images/map_point.png",
                    "iconImageSize" : [20, 20],
                    "iconImageOffset": [-10, -10]
                }
            },
            {"type": "Feature", "id": 1, "geometry": {"type": "Point", "coordinates": [55.931903, 37.511961]},
                "properties": {
                    // "balloonContentHeader": "Это balloonContentHeader",
                    "balloonContentBody": "<div class='shop-map__balloon-body'>Контент</dib>",
                    // "balloonContentFooter": "это balloonContentFooter",
                    "hintContent": "Это hintContent"
                },
                "options" : {
                    "iconLayout" : "default#image",
                    "iconImageHref" : "images/map_point.png",
                    "iconImageSize" : [20, 20],
                    "iconImageOffset": [-10, -10]
                }
            },
        ]
    };

    objectManager.add(data_points);

    // добавим событие на клик по метке
    myMap.geoObjects.events.add('click', function (e,e2) {
        var id = e.get('objectId');
        $('.shop-nearby-change').load('shop-nearby' + id + '.html .shop-nearby');
    });

    myMap.geoObjects.add(objectManager);

    // Условие если планшет
    if ($(window).innerWidth() < 1260) {
        var resultQuery = ymaps.geoQuery(data_points);
            // console.log(resultQuery);
        
        location.get().then(
            function (res) {
                // Добавление местоположения на карту.
                myMap.geoObjects.add(res.geoObjects);
                var firstGeoObject = res.geoObjects.get(0), 
                    bounds = firstGeoObject.properties.get('coords');
                console.log(resultQuery.getClosestTo(firstGeoObject));
                // myMap.setBounds(bounds, {
                //         checkZoomRange: true
                // });
            },
            function(err) {
                console.log('Ошибка: ' + err)
            });
    }

    $('.shops-map__loading').hide();
}

