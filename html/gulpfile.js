'use strict';
let gulp         = require('gulp');
let prefixer     = require('gulp-autoprefixer');
let uglify       = require('gulp-uglify');
let sass         = require('gulp-sass');
let sourcemaps   = require('gulp-sourcemaps');
let fileinclude = require('gulp-file-include');
let cssmin       = require('gulp-clean-css');
let imagemin     = require('gulp-imagemin');
let rimraf       = require('rimraf');
let browserSync  = require("browser-sync");
let cache		 = require('gulp-cached');
let reload       = browserSync.reload;
let plumber      = require('gulp-plumber');
let notify       = require('gulp-notify');
let babel        = require('gulp-babel');

let path = {
	build: {
		html: 'build/',
		php: 'build/',
		js: 'build/js/',
		css: 'build/css/',
		img: 'build/images/',
		svg: 'build/images/icons/',
		fonts: 'build/fonts/',
		modulesStatic: 'build/modules/',
		modulesCSS: 'build/modules/',
		modulesJS: 'build/modules/',
		json: 'build/',
		document: 'build/'
	},
	src: {
		html: 'src/*.html',
		php: 'src/*.php',
		js: ['src/js/main.js', '!src/modules/swiper/swiper.js'],
		style: ['src/style/main.scss', 'src/style/debug.scss'],
		img_1: ['src/images/*', '!src/images/icons/*', '!src/images/**/*.psd', '!src/images/**/*.svg'],
		img_2: ['src/images/*/**/*', '!src/images/icons/*', '!src/images/**/*.psd', '!src/images/**/*.svg'],
		fonts: 'src/fonts/**/*.*',
		svg: 'src/images/icons/**/*.svg',
		modulesStatic: ['src/modules/**/*.html', 'src/modules/**/*.php', 'src/modules/**/*.json'],
		modulesCSS: ['src/modules/**/*.scss'],
		modulesJS: ['src/modules/**/*.js', 'src/modules/**/*.jsx'],
		json: 'src/**/*.json',
		document: 'src/**/*.mp3'
	},
	watch: {
		html: ['src/*.html', 'src/includes/*.html'],
		php: 'src/*.php',
		js: 'src/js/**/*.js',
		style: ['src/style/**/*.scss'],
		img: ['src/images/**/', 'src/images/*'],
		svg: 'src/images/icons/**/*.svg',
		fonts: 'src/fonts/**/*.*',
		modulesStatic: ['src/modules/**/*.html', 'src/modules/**/*.json', 'src/modules/**/*.php'],
		modulesCSS: ['src/modules/**/*.scss'],
		modulesJS: ['src/modules/**/*.js', 'src/modules/**/*.jsx']
	},
	clean: './build',
	map: {
		js: 'maps/',
		css: 'maps/'
	},
	template: {
		css: '../local/templates/promet/css/',
		js: '../local/templates/promet/js/',
		fonts: '../local/templates/promet/fonts/',
		img: '../local/templates/promet/images/',
		svg: '../local/templates/promet/images/icons/',
		modulesStatic: '../local/templates/promet/modules/',
		modulesCSS: '../local/templates/promet/modules/',
		modulesJS: '../local/templates/promet/modules/'
	}
};
let server_config = {
		server: {
			baseDir: "./build/"
		},
		tunnel: true,
		host: 'localhost',
		port: 9900,
		open: false,
		logPrefix: "build",
	},
	task_delay = 500;

// Base tasks
gulp.task('html:build', function () {
	setTimeout(function () {
		gulp.src(path.src.html)
			.pipe(plumber({errorHandler: notify.onError("Error: <%= error.message %>")}))
			.pipe(fileinclude({
				prefix: '@@',
				basepath: '@file'
			}))
			.pipe(gulp.dest(path.build.html))
			.pipe(plumber.stop())
			.pipe(reload({stream: true}));
	}, task_delay);
});
gulp.task('php:build', function () {
	setTimeout(function () {
		gulp.src(path.src.php)
			.pipe(gulp.dest(path.build.php))
			.pipe(reload({stream: true}));
	}, task_delay);
});
gulp.task('json:build', function () {
	setTimeout(function () {
		gulp.src(path.src.json)
			.pipe(gulp.dest(path.build.json))
			.pipe(reload({stream: true}));
	}, task_delay);
});
gulp.task('document:build', function () {
	setTimeout(function () {
		gulp.src(path.src.document)
			.pipe(gulp.dest(path.build.json))
	}, task_delay);
});
gulp.task('js:build', function () {
	setTimeout(function () {
		gulp.src(path.src.js)
			.pipe(plumber({errorHandler: notify.onError("Error: <%= error.message %>")}))
			.pipe(fileinclude({
				prefix: '@@',
				basepath: '@file'
			}))
			.pipe(gulp.dest(path.build.js))
			.pipe(gulp.dest(path.template.js))
			.pipe(reload({stream: true}));
	}, task_delay);
});
gulp.task('js:build:prod', function () {
	setTimeout(function () {
		gulp.src(path.src.js)
			.pipe(plumber({errorHandler: notify.onError("Error: <%= error.message %>")}))
			.pipe(fileinclude({
				prefix: '@@',
				basepath: '@file'
			}))
			.pipe(babel({
				presets: ['@babel/env'],
				ignore: ['src/modules/swiper/swiper.js']
			}))
			.pipe(uglify())
			.pipe(gulp.dest(path.build.js))
			.pipe(gulp.dest(path.template.js))
			.pipe(reload({stream: true}));
	}, task_delay);
});
gulp.task('style:build', function () {
	setTimeout(function () {
		gulp.src(path.src.style)
			.pipe(plumber({errorHandler: notify.onError("Error: <%= error.message %>")}))
			.pipe(fileinclude({
				prefix: '@@',
				basepath: '@file'
			}))
			.pipe(sass().on('error', sass.logError))
			.pipe(prefixer())
			.pipe(cssmin())
			.pipe(gulp.dest(path.build.css))
			.pipe(gulp.dest(path.template.css))
			.pipe(reload({stream: true}));
	}, task_delay);
});
gulp.task('style:build:prod', function () {
	setTimeout(function () {
		gulp.src(path.src.style)
			.pipe(plumber({errorHandler: notify.onError("Error: <%= error.message %>")}))
			.pipe(fileinclude({
				prefix: '@@',
				basepath: '@file'
			}))
			.pipe(sass().on('error', sass.logError))
			.pipe(prefixer())
			.pipe(cssmin())
			.pipe(gulp.dest(path.build.css))
			.pipe(gulp.dest(path.template.css))
			.pipe(reload({stream: true}));
	}, task_delay);
});

gulp.task('image:build', ['image:folders:build'], function () {
		gulp.src(path.src.img_1)
			.pipe(cache('images'))
			.pipe(imagemin([
				imagemin.gifsicle({interlaced: true}),
				imagemin.jpegtran({progressive: true}),
				imagemin.optipng({optimizationLevel: 3}),
				imagemin.svgo({
					plugins: [
						{removeViewBox: true},
						{cleanupIDs: false}
					]
				})
			]))
			.pipe(gulp.dest(path.build.img))
			.pipe(gulp.dest(path.template.img))
});
gulp.task('image:folders:build', function () {
	setTimeout(function () {
		gulp.src(path.src.img_2)
			.pipe(cache('images'))
			.pipe(imagemin([
				imagemin.gifsicle({interlaced: true}),
				imagemin.jpegtran({progressive: true}),
				imagemin.optipng({optimizationLevel: 3}),
				imagemin.svgo({
					plugins: [
						{removeViewBox: true},
						{cleanupIDs: false}
					]
				})
			]))
			.pipe(gulp.dest(path.build.img));
	}, task_delay);
});
gulp.task('svg:copy:build', function () {
	gulp.src(path.src.svg)
		.pipe(gulp.dest(path.build.svg))
});
gulp.task('fonts:build', function() {
	setTimeout(function () {
		gulp.src(path.src.fonts)
			.pipe(gulp.dest(path.build.fonts))
			.pipe(gulp.dest(path.template.fonts));
	}, task_delay);
});

// Modules tasks
gulp.task('modules:static', function () {
	return gulp.src(path.src.modulesStatic)
		.pipe(gulp.dest(path.build.modulesStatic))
		.pipe(reload({stream: true}));
});
gulp.task('modules:css', function () {
	return gulp.src(path.src.modulesCSS)
		.pipe(sass.sync().on('error', sass.logError))
		.pipe(plumber({errorHandler: notify.onError("Error: <%= error.message %>")}))
		.pipe(prefixer({
			browsers: ['last 12 versions','> 1%'],
			cascade: false,
			remove: false
		}))
		.pipe(cssmin())
		.pipe(plumber.stop())
		.pipe(gulp.dest(path.build.modulesCSS))
		.pipe(gulp.dest(path.template.modulesCSS))
		.pipe(reload({stream: true}));
});
gulp.task('modules:js', function() {
	setTimeout(function () {
		gulp.src(path.src.modulesJS)
			.pipe(plumber())
			.pipe(babel({
				presets: ['@babel/env'],
				ignore: ['src/modules/swiper/swiper.js']
			}))
			.pipe(uglify())
			.pipe(gulp.dest(path.build.modulesJS))
			.pipe(gulp.dest(path.template.modulesJS))
			.pipe(reload({stream: true}));
	}, task_delay);
});
gulp.task('build', [
	'html:build',
	'php:build',
	'js:build',
	'style:build',
	'image:build',
	'svg:copy:build',
	'fonts:build',
	'modules:static',
	'json:build',
	'document:build',
	'modules:css',
	'modules:js',
]);
gulp.task('build:js:css', [
	'js:build',
	'style:build',
	'fonts:build',
	'modules:css',
	'modules:js'
]);
gulp.task('build:js:css:prod', [
	'js:build:prod',
	'style:build:prod',
	'fonts:build',
	'modules:css',
	'modules:js'
]);
gulp.task('watch', function(){
	gulp.watch(path.watch.html,['html:build']);
	gulp.watch(path.watch.php,['php:build']);
	gulp.watch(path.watch.style,['style:build']);
	gulp.watch(path.watch.modulesCSS,['modules:css']);
	gulp.watch(path.watch.js,['js:build']);
	gulp.watch(path.watch.img,['image:build']);
	gulp.watch(path.watch.svg,['svg:copy:build']);
	gulp.watch(path.watch.fonts,['fonts:build']);
	gulp.watch(path.watch.modulesStatic,['modules:static', 'html:build', 'php:build']);
	gulp.watch(path.watch.modulesJS,['modules:js']);
});
gulp.task('watch:js:css', function(){
	gulp.watch(path.watch.style,['style:build']);
	gulp.watch(path.watch.js,['js:build']);
	gulp.watch(path.watch.fonts,['fonts:build']);
	gulp.watch(path.watch.modulesCSS,['modules:css']);
	gulp.watch(path.watch.modulesJS,['modules:js']);
});
gulp.task('webserver:tunnel:true', function () {
	server_config.tunnel = true;
	browserSync(server_config);
});
gulp.task('webserver:tunnel:false', function () {
	server_config.tunnel = false;
	browserSync(server_config);
});
gulp.task('clean', function (cb) {
	rimraf(path.clean, cb);
});
gulp.task('default', ['build', 'webserver:tunnel:false', 'watch']);
gulp.task('default:tunnel:true', ['build', 'webserver:tunnel:true', 'watch']);
gulp.task('js:css', ['build:js:css', 'watch:js:css']);
